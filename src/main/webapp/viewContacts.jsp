<%-- 
    Document   : viewContacts
    Created on : 09-Sep-2021, 8:33:28 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>        
        <jsp:include page="/WEB-INF/jsps/header.jsp"/>
        <br/>

        <c:choose>
            <c:when test="${not empty requestScope.contactList}">
                <table border="1" cellpadding="3px" cellspacing="3px">
                    <tr><td>ID</td><td>Name</td><td>Email</td><td>Mobile</td><td>Ops</td></tr>

                    <c:forEach items="${requestScope.contactList}" var="temp">
                        <tr>
                            <td>${temp.contactId}</td>
                            <td>${temp.name}</td>
                            <td>${temp.email}</td>
                            <td>${temp.mobile}</td>
                            <td>
                                <a href="updateContact?contactId=${temp.contactId}">Update</a>
                                <a href="deleteContact?contactId=${temp.contactId}">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <h3>you have no contacts yet<h3>
            </c:otherwise>
        </c:choose>
    </body>
</html>
