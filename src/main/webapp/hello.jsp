
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="java.time.*, java.util.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h1><%= LocalDateTime.now()%></h1>
        <h2>
            <%= request.getParameter("tech-name")%>

            <%
                out.println("<h1>SCRIPTLET: " + LocalDateTime.now() + "</h1>");
            %>
        </h2>
    </body>
</html>
