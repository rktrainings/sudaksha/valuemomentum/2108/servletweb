
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <sql:query dataSource="${applicationScope.dataSource}"
                   sql="SELECT * FROM users" var="users"/>

        <table border="1" cellspacing="3px" cellpadding="3px">
            <tr>
                <td>ID</td><td>Name</td><td>Email</td><td>Remarks</td>
            </tr>

            <c:forEach items="${users.rowsByIndex}" var="temp">
                <tr>
                    <td>${temp[0]}</td>
                    <td>${fn:toUpperCase(temp[1])}</td>
                    <td>${temp[2]}</td>
                    <td>${temp[3]}</td>
                </tr>
            </c:forEach>

        </table>
    </body>
</html>
