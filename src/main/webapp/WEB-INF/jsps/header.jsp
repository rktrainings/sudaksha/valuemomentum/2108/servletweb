<%-- 
    Document   : header
    Created on : 09-Sep-2021, 8:28:15 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${not empty sessionScope.userId}">
        <a style="margin-right: 10px; text-decoration: none; font-size: 18px" href="home.jsp">Home</a>
        <a style="margin-right: 10px; text-decoration: none; font-size: 18px" href="viewContacts">View Contacts</a>
        <a style="margin-right: 10px; text-decoration: none; font-size: 18px" href="addContact.jsp">Add Contact</a>
        <a style="margin-right: 10px; text-decoration: none; font-size: 18px" href="logout">Logout</a>
        
        <span style="color: darkgreen; float:right; font-size:18px">
            JSESSIONID: <% 
                out.println(session.getId());
                %>&nbsp;&nbsp;&nbsp;&nbsp;
            Hi, ${sessionScope.userName}
        </span>
    </c:when>
    <c:otherwise>
        <a href="login.html">Login</a>
    </c:otherwise>
</c:choose>


