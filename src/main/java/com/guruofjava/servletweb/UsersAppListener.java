package com.guruofjava.servletweb;

import java.sql.SQLException;
import java.util.Objects;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

public class UsersAppListener implements ServletContextListener {

    private DataSource ds;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        BasicDataSource commonsDS = new BasicDataSource();
        commonsDS.setDriverClassName("com.mysql.jdbc.Driver");
        commonsDS.setUrl("jdbc:mysql://45.120.136.152:3306/vrk2108");
        commonsDS.setUsername("vrk2108");
        commonsDS.setPassword("vrk2108");

        ds = commonsDS;
        
        sce.getServletContext().setAttribute("dataSource", ds);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (Objects.nonNull(ds)) {
            BasicDataSource temp = (BasicDataSource) ds;
            try {
                temp.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
