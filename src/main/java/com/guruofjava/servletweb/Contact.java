package com.guruofjava.servletweb;

public class Contact {

    private Integer contactId;
    private String name;
    private String email;
    private String mobile;

    public Contact() {
        super();
    }

    public Contact(Integer contactId, String name, String email, String mobile) {
        this.contactId = contactId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
