package com.guruofjava.servletweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        
        HttpSession sesssion = null;
        ServletContext ctx = getServletContext();

        PrintWriter out = resp.getWriter();

        out.println("GET: " + LocalDateTime.now());
        out.println("<br/>Tech Name: " + req.getParameter("tech-name"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();

        out.println("POST: " + LocalDateTime.now());
        out.println("<br/>Tech Name: " + req.getParameter("tech-name"));
    }

}
