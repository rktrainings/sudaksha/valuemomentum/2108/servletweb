package com.guruofjava.servletweb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

public class ContactsServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        Integer userId = null;

        if (Objects.nonNull(session)) {
            userId = (Integer) session.getAttribute("userId");
        }

        if (Objects.nonNull(userId)) {
            DataSource ds = (DataSource) getServletContext().getAttribute("dataSource");

            try ( Connection con = ds.getConnection();  PreparedStatement pst = con.prepareStatement("SELECT * FROM contacts WHERE user_id=?");) {

                pst.setInt(1, userId);
                ResultSet rs = pst.executeQuery();

                List<Contact> contacts = new ArrayList();
                while (rs.next()) {
                    contacts.add(new Contact(rs.getInt(1), rs.getString(2), rs.getString(3),
                            rs.getString(4)));
                }

                request.setAttribute("contactList", contacts);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        request.getRequestDispatcher("viewContacts.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
