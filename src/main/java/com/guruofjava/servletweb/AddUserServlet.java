package com.guruofjava.servletweb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public class AddUserServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String name = request.getParameter("name");
        String email = request.getParameter("mail");
        String remarks = request.getParameter("remarks");
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception ex) {
            
        }
        
        DataSource ds = (DataSource) getServletContext().getAttribute("dataSource");
        
        try ( Connection con = ds.getConnection();  PreparedStatement pst = con.prepareStatement("INSERT INTO users VALUES(0, ?, ?, ?)");) {
            
            pst.setString(1, name);
            pst.setString(2, email);
            pst.setString(3, remarks);
            pst.executeUpdate();
            
            response.sendRedirect("viewUsers2.jsp");
            //request.getRequestDispatcher("viewUsers2.jsp").forward(request, response);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
}
