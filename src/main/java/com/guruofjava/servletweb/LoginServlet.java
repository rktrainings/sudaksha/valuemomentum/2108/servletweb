package com.guruofjava.servletweb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

public class LoginServlet extends HttpServlet {
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("email");
        String password = request.getParameter("pwd");
        
        if (!username.equals(password)) {
            response.sendRedirect("login.html");
            return;
        }
        
        DataSource ds = (DataSource) getServletContext().getAttribute("dataSource");
        
        try ( Connection con = ds.getConnection();  PreparedStatement pst = con.prepareStatement("SELECT * FROM users where email=?");) {
            pst.setString(1, username);
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                HttpSession session = request.getSession(true);
                session.setAttribute("userId", rs.getInt(1));
                session.setAttribute("userName", rs.getString(2));
                
                response.sendRedirect("home.jsp");
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
