package com.guruofjava.servletweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class TimeServlet extends GenericServlet{
    
    @Override
    public void service(ServletRequest request, ServletResponse response)throws IOException, ServletException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        ServletConfig config = this.getServletConfig();
        String datePattern = config.getInitParameter("date-pattern");
        
        LocalDate today = LocalDate.now();
        
        String name = request.getParameter("name");
        String email = request.getParameter("mail");
        
        out.println("<html><body>");
        out.println("Hi " + name + ", It's now: " + new Date());
        out.println("<br/>");
        out.println("Your mail id is: " + email);
        out.println("<br/>Today: " + today.format(DateTimeFormatter.ofPattern(datePattern)));
        out.println("</body></html>");
    }
}
