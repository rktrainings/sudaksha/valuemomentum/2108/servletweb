package com.guruofjava.servletweb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

public class DeleteContactServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        Integer userId = null;

        if (Objects.nonNull(session)) {
            userId = (Integer) session.getAttribute("userId");
        }

        if (Objects.nonNull(userId)) {
            DataSource ds = (DataSource) getServletContext().getAttribute("dataSource");

            try ( Connection con = ds.getConnection();  PreparedStatement pst = con.prepareStatement("DELETE FROM contacts WHERE user_id=? and contact_id=?");) {

                pst.setInt(1, userId);
                pst.setInt(2, Integer.parseInt(request.getParameter("contactId")));
                pst.executeUpdate();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        request.getRequestDispatcher("viewContacts").forward(request, response);
    }
}
